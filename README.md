# CoR

## Стэк технологий
- Docker
- PHP 8.1
- Nginx
- Symfony 6

## Установка проекта
### 1. ENV
Скопируйте .env.example в .env 

### 2. docker-compose
Проект работает с системой Docker. Запустите команду `docker compose up -d`

### 3. Composer
`docker exec cor_php composer install`

## Технические команды
### 1. Обработка запроса от клиента в middleware 
`docker exec -it cor_php php bin/console corPattern
`
