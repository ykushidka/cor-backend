<?php
declare(strict_types=1);

namespace App\Command;

use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use App\Cor\FilterMiddleware;
use App\DBAL\Enum\General\Role;
use App\DBAL\Enum\General\Status;

#[AsCommand(
    name: 'corPattern'
)]
class MiddlewareCommand extends Command
{
    private array $filterMiddleware = [];

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $file = file_get_contents('users.json');
        $users = json_decode($file, TRUE);
        unset($file);

        $output->writeln([
            '============================',
            'Enter data for verification:',
            '============================'
        ]);

        $this->getValueForMiddleware(new SymfonyStyle($input, $output));

        $user = array_filter($users, function ($item) {
            return $item['id'] === $this->filterMiddleware['id'];
        });

        if (empty($user[0])) {
            $output->writeln(['User does not exist']);
            return Command::FAILURE;
        }

        $filter = new FilterMiddleware();
        $filter->setFilter($this->filterMiddleware);
        $filter->initValidateMiddleware();
        $res = $filter->check($user[0]);

        if (!$res) $output->writeln(['User did not pass by condition']);

        return Command::SUCCESS;
    }

    private function getValueForMiddleware(SymfonyStyle $io): void
    {
        $this->filterMiddleware = [
            'id' => $io->ask('Enter user ID', null, function ($value) {
                $value = (int)$value;
                if ($value === 0) throw new \Exception('Wrong format, try again');
                return $value;
            }),
            'status' => $io->choice('Select status', [
                Status::ACTIVE,
                Status::NOT_ACTIVE,
            ]),
            'role' => $io->choice('Select role', [
                Role::ADMIN,
                Role::MANAGER,
                Role::AGENT
            ]),
            'permission' => $io->choice('Select permission', [
                'true',
                'false'
            ])
        ];
    }
}
