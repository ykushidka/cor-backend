<?php
declare(strict_types=1);

namespace App\Cor;

use App\DBAL\Enum\General\Role;
use App\DBAL\Enum\General\ExcessiveActivity;

class ExcessiveActivityMiddleware extends Middleware
{
    public function check(array $data): bool
    {
        switch ($data['role']) {
            case  Role::ADMIN:
                if ($data['permission']) {
                    if ($data['excessiveActivity'] >= ExcessiveActivity::ADMIN_TRUE_PERMISSION) return false;
                } else {
                    if ($data['excessiveActivity'] >= ExcessiveActivity::ADMIN_FALSE_PERMISSION) return false;
                }
                break;
            case Role::MANAGER:
                if ($data['permission']) {
                    if ($data['excessiveActivity'] >= ExcessiveActivity::MANAGER_TRUE_PERMISSION) return false;
                } else {
                    if ($data['excessiveActivity'] >= ExcessiveActivity::MANAGER_FALSE_PERMISSION) return false;
                }
                break;
            case Role::AGENT:
                if ($data['permission']) {
                    if ($data['excessiveActivity'] >= ExcessiveActivity::AGENT_TRUE_PERMISSION) return false;
                } else {
                    if ($data['excessiveActivity'] >= ExcessiveActivity::AGENT_FALSE_PERMISSION) return false;
                }
                break;
        }
        return parent::check($data);
    }
}
