<?php
declare(strict_types=1);

namespace App\Cor;

class FilterMiddleware
{
    private string $status;
    private string $role;
    private bool $permission;
    private Middleware $middleware;

    public function initValidateMiddleware(): void
    {
        $middleware = new StatusMiddleware($this->status);
        $middleware
            ->nextStep(new RoleMiddleware($this->role))
            ->nextStep(new PermissionMiddleware($this->permission))
            ->nextStep(new ExcessiveActivityMiddleware());
        $this->middleware = $middleware;
    }

    public function setFilter(array $data)
    {
        $this->status = $data['status'];
        $this->role = $data['role'];
        $this->permission = (bool)$data['permission'];
    }

    public function check(array $data): bool
    {
        return $this->middleware->check($data);
    }
}
