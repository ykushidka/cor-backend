<?php
declare(strict_types=1);

namespace App\Cor;

abstract class Middleware
{
    /**
     * @var Middleware
     */
    private $next;

    public function nextStep(Middleware $next): Middleware
    {
        $this->next = $next;
        return $next;
    }

    public function check(array $data): bool
    {
        if (!$this->next) return true;

        return $this->next->check($data);
    }
}
