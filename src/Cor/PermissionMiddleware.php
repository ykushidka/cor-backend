<?php
declare(strict_types=1);

namespace App\Cor;

class PermissionMiddleware extends Middleware
{
    public function __construct(private bool $permission)
    {
    }

    public function check(array $data): bool
    {
        if ($this->permission !== $data['permission']) return false;

        return parent::check($data);
    }
}
