<?php
declare(strict_types=1);

namespace App\Cor;

class RoleMiddleware extends Middleware
{
    public function __construct(private string $role)
    {
    }

    public function check(array $data): bool
    {
        if ($this->role !== $data['role']) return false;

        return parent::check($data);
    }
}
