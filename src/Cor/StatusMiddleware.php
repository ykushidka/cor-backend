<?php
declare(strict_types=1);

namespace App\Cor;

class StatusMiddleware extends Middleware
{
    public function __construct(private string $status)
    {
    }

    public function check(array $data): bool
    {
        if ($this->status !== $data['status']) return false;

        return parent::check($data);
    }
}
