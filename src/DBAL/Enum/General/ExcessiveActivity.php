<?php

namespace App\DBAL\Enum\General;

class ExcessiveActivity
{
    public const ADMIN_TRUE_PERMISSION = 100;
    public const ADMIN_FALSE_PERMISSION = 90;
    public const MANAGER_TRUE_PERMISSION = 70;
    public const MANAGER_FALSE_PERMISSION = 50;
    public const AGENT_TRUE_PERMISSION = 30;
    public const AGENT_FALSE_PERMISSION = 10;
}
