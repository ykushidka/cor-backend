<?php

namespace App\DBAL\Enum\General;

final class Role
{
    public const ADMIN = 'ROLE_ADMIN';
    public const MANAGER = 'ROLE_MANAGER';
    public const AGENT = 'ROLE_AGENT';
}
