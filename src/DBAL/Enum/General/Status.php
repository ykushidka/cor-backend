<?php

namespace App\DBAL\Enum\General;

final class Status
{
    public const ACTIVE = 'ACTIVE';
    public const NOT_ACTIVE = 'NOT_ACTIVE';
}
